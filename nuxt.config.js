const pkg = require('./package')
const webpack = require("webpack");


module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', type: 'text/css'},
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'},
    ],
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/main.css',
    '@/assets/transition.css',
    '@/assets/js/bootstrap/css/all.css',
    '@/assets/js/bootstrap/css/bootstrap.css',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/vue-loading-overlay.js', ssr: true },
    { src: '~plugins/vue-transition.js', ssr: false },
    '@/plugins/bootstrap',
    '@/plugins/popper',
    '~/plugins/axios',
    '~/plugins/func',
    { src: '~/plugins/json-id.js', ssr: false},
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/moment',
    'cookie-universal-nuxt',
    ['@nuxtjs/dotenv'],
  ],
  // AXIOS CONFIGURATION
  axios: {
    // ---------- maunual setting ---------
    // proxyHeaders: false,
    // credentials: false,
    // baseURL: 'http://api.edutore.com:3000/api/v1/'
    // baseURL: 'https://api.edutore.com/api/v1/',
    // baseURL: 'https://10.148.0.6:3000/api/v1/'
    baseURL: process.env.NODE_ENV !== "production"
      ? `https://api.edutore.com/api/v1/`
      : "https://api.edutore.com/api/v1/",
    // ---------- proxy setting -----------
    // proxy: true, // Can be also an object with default options
    // prefix: '/api/'
  },
  /*
  ** Build configuration
  */
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0', // default: localhost,
  },
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery"
      })
    ],
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
