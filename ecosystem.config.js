module.exports = {
  apps : [
    {
      name: "edu-web-belajar-gratis-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "edu-web-belajar-gratis-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "edu-web-belajar-gratis-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
