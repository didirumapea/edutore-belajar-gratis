$(document).ready(function() {
  /** Multiple Dropdown */
  $(".navbar .dropdown-item").on("click", function(e) {
    var $el = $(this).children(".dropdown-toggle");
    var $parent = $el.offsetParent(".dropdown-menu");
    $(this)
      .parent("li")
      .toggleClass("open");

    if (!$parent.parent().hasClass("navbar-nav")) {
      if ($parent.hasClass("show")) {
        $parent.removeClass("show");
        $el.next().removeClass("show");
        $el.next().css({ top: -999, left: -999 });
      } else {
        $parent
          .parent()
          .find(".show")
          .removeClass("show");
        $parent.addClass("show");
        $el.next().addClass("show");
        $el
          .next()
          .css({ top: $el[0].offsetTop, left: $parent.outerWidth() - 4 });
      }
      e.preventDefault();
      e.stopPropagation();
    }
  });

  $(".navbar .dropdown").on("hidden.bs.dropdown", function() {
    $(this)
      .find("li.dropdown")
      .removeClass("show open");
    $(this)
      .find("ul.dropdown-menu")
      .removeClass("show open");
  });

  /** Navbar Scrolldown Hide */
  var zero = 0;
  $(window).on("scroll", function() {
    $(".navbar").toggleClass("hide", $(window).scrollTop() > zero);
    zero = $(window).scrollTop();
  });
});

/** Show More */
// Add click event dynamically
$(document).on("click", ".toggle-text-button", function() {
  // Check if text is more or less
  if ($(this).text() == "Selengkapnya...") {
    // Change link text
    $(this).text("Tutup");

    // Travel up DOM tree to parent, then find any children with CLASS .toggle-text and slide down
    $(this)
      .parent()
      .children(".toggle-text")
      .slideDown();
  } else {
    // Change link text
    $(this).text("Selengkapnya...");

    // Travel up DOM tree to parent, then find any children with CLASS .toggle-text and slide up
    $(this)
      .parent()
      .children(".toggle-text")
      .slideUp();
  }
});
